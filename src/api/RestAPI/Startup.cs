using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Services;
using RedisStores;
using RestAPI.Middlewares.AuthenticationHandlers;
using RestAPI.Middlewares.Swagger;
using NSQPublishers;
using System.Text.Json;
using RestAPI.Middlewares;
using Microsoft.AspNetCore.Http;

namespace RestAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RestAPI", Version = "v1" });

                c.AddServer(new OpenApiServer()
                {
                    Url = "https://localhost/api/",
                    Description = "Local Development Environment",
                });

                c.OperationFilter<SecurityDefinitionOperationFilter>();

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "Bearer",
                    BearerFormat = "string",
                    Description = "Bearer Authentication",
                    In = ParameterLocation.Header,
                });
            });


            services.AddRedisChannelStore(Configuration.GetValue<string>("RedisConfig"));
            services.AddNSQEventPublisher(Configuration.GetValue<string>("NSQDHost"));
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Bearer";
                options.AddScheme<CustomBearerAuthenticationHandler>("Bearer", "Bearer");
            });
            services.AddChannelService();
            services.AddMessageService();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.Use((context, next) =>
            {
                context.Request.PathBase = "/api";
                return next();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/api/swagger/v1/swagger.json", "RestAPI v1"));
            }

            app.UseHealthMiddleware();

            // app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
