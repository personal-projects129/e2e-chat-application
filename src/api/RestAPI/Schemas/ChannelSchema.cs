

namespace RestAPI.Schemas {

    public class ChannelSchema {

        public string ChannelID { get; set; }

        public string Challenge { get; set; }

        public string Token { get; set; }
    }
}