

namespace RestAPI.Schemas {

    public class MemberSchema {

        public string DisplayName { get; set; }

        public bool Active { get; set; }
    }
}