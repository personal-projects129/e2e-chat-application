
using Microsoft.AspNetCore.Mvc;
using RestAPI.Schemas;
using Microsoft.Extensions.Logging;
using Services;
using Services.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using System.Net.Http;


namespace RestAPI.Controllers
{

    [ApiController]
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ChannelsController : Controller
    {

        private ILogger _logger;
        private ChannelService _channelService;
        public ChannelsController(ILogger<ChannelsController> logger, ChannelService channelService)
        {
            _logger = logger;
            _channelService = channelService;
        }

        [HttpPost]
        public async Task<ActionResult<ResponseSchema<ChannelSchema>>> NewChannel(ChannelSchema newChannel)
        {

            ServiceResult<ChannelModel> result;
            try
            {
                result = await _channelService.CreateNewChannel(new ChannelModel
                {
                    Challenge = newChannel.Challenge,
                    Token = newChannel.Token,
                });
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                _logger.LogCritical(e.StackTrace);
                return Problem("Server Issue Occured");
            }

            HttpContext.Response.StatusCode = result.Code;
            return new ResponseSchema<ChannelSchema>
            {
                Data = new ChannelSchema
                {
                    Challenge = result.Data?.Challenge,
                    ChannelID = result.Data?.ChannelID
                },
                Message = result.Message,
                StatusCode = result.Code
            };
        }

        [HttpGet]
        [Route("{channelID}")]
        public async Task<ActionResult<ResponseSchema<ChannelSchema>>> GetChannel(string channelID)
        {
            ServiceResult<ChannelModel> result;

            try
            {
                result = await _channelService.GetChannel(channelID);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                _logger.LogCritical(e.StackTrace);
                return Problem("Server Issue Occured");
            }

            HttpContext.Response.StatusCode = result.Code;
            return new ResponseSchema<ChannelSchema>
            {
                Data = new ChannelSchema
                {
                    Challenge = result.Data?.Challenge,
                    ChannelID = result.Data?.ChannelID
                },
                Message = result.Message,
                StatusCode = result.Code
            };
        }

        [HttpDelete]
        [Route("{channelID}")]
        [Authorize]
        public async Task<ActionResult<ResponseSchema<ChannelSchema>>> DeleteChannel(string channelID)
        {
            ServiceResult<ChannelModel> result;

            try
            {
                result = await _channelService.DeleteChannel(channelID);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                _logger.LogCritical(e.StackTrace);
                return Problem("Server Issue Occured");
            }

            HttpContext.Response.StatusCode = result.Code;
            return new ResponseSchema<ChannelSchema>
            {
                Data = new ChannelSchema
                {
                    Challenge = result.Data?.Challenge,
                    ChannelID = result.Data?.ChannelID
                },
                Message = result.Message,
                StatusCode = result.Code
            };
        }

    }
}