using Microsoft.AspNetCore.Mvc;
using RestAPI.Schemas;
using Microsoft.AspNetCore.Authorization;
using Services;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System;
using Services.Models;

using System.Security.Claims;

namespace RestAPI.Controllers
{

    [ApiController]
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/channels/{channelID}/[controller]")]
    public class MessagesController : Controller
    {

        private ILogger _logger;

        private MessageService _messageService;

        public MessagesController(ILogger<MessagesController> logger, MessageService messageService)
        {
            _logger = logger;
            _messageService = messageService;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<ResponseSchema<MessageSchema>>> PostMessage(MessageSchema newMessage)
        {
            newMessage.ChannelID = User.FindFirst("channelID").Value;
            newMessage.From = User.FindFirst(ClaimTypes.Name).Value;

            ServiceResult<MessageModel> result;
            try
            {
                result = await _messageService.SendMessage(new MessageModel
                {
                    EncryptedMessage = newMessage.EncryptedMessage,
                    Fourth = newMessage.Fourth,
                    From = newMessage.From,
                    ChannelID = User.FindFirst("channelID").Value,
                });
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                _logger.LogCritical(e.StackTrace);

                return Problem($"Server Issue Occured.");
            }

            HttpContext.Response.StatusCode = result.Code;
            return new ResponseSchema<MessageSchema>
            {
                Data = new MessageSchema
                {
                    ChannelID = result.Data?.ChannelID,
                    EncryptedMessage = result.Data?.EncryptedMessage,
                    Fourth = result.Data?.Fourth,
                    From = result.Data?.From,
                    TimeStamp = result.Data?.TimeStamp ?? 0
                },
                Message = result.Message,
                StatusCode = result.Code
            };
        }
    }

}