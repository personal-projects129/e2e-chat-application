
using System;

namespace RedisStores.Exceptions
{

    public class FailedToSetKeyException : Exception
    {
        public FailedToSetKeyException(string message) : base(message) { }
    }
}