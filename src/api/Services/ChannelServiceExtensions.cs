
using Microsoft.Extensions.DependencyInjection;

namespace Services
{

    public static class ChannelServiceExtensions
    {
        public static IServiceCollection AddChannelService(this IServiceCollection service)
        {
            service.AddSingleton<ChannelService>();
            return service;
        }
    }
}