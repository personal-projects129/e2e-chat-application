

namespace Services.Models
{

    public class ServiceResult<T>
    {

        public int Code { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }
    }
}