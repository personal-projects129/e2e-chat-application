


namespace Services.Models
{

    public class ChannelModel
    {

        public string ChannelID { get; set; }

        public string Challenge { get; set; }

        public string Token { get; set; }

    }
}