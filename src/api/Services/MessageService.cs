
using Microsoft.Extensions.Logging;
using Services.Publishers;
using Services.Models;
using System.Threading.Tasks;
using System;

namespace Services
{


    public class MessageService
    {


        private ILogger _logger;
        private IEventPublisher _publisher;

        public MessageService(ILogger<MessageService> logger, IEventPublisher eventPublisher)
        {
            _logger = logger;
            _publisher = eventPublisher;
        }

        public async Task<ServiceResult<MessageModel>> SendMessage(MessageModel message)
        {

            if (message.From == "" || message.From == null)
            {
                return new ServiceResult<MessageModel>
                {
                    Code = 400,
                    Data = message,
                    Message = "from is required",
                };
            }

            if (message.From.Length < 5)
            {
                return new ServiceResult<MessageModel>
                {
                    Code = 400,
                    Data = message,
                    Message = "from should be atleast 5 characters"
                };
            }

            if (message.From.Length > 20)
            {
                return new ServiceResult<MessageModel>
                {
                    Code = 400,
                    Data = message,
                    Message = "from should be atmost 20 characters",
                };
            }

            if (message.EncryptedMessage == "" || message.EncryptedMessage == null)
            {
                return new ServiceResult<MessageModel>
                {
                    Code = 400,
                    Message = "encryptedMessage is required"
                };
            }

            try
            {
                Convert.FromBase64String(message.EncryptedMessage);
            }
            catch (FormatException)
            {
                return new ServiceResult<MessageModel>
                {
                    Code = 400,
                    Message = "encryptedMessage is not a valid base64 string"
                };
            }

            if (message.Fourth == "" || message.Fourth == null)
            {
                return new ServiceResult<MessageModel>
                {
                    Code = 400,
                    Message = "fourth is required",
                };
            }

            try
            {
                var fourthBytes = Convert.FromBase64String(message.Fourth);
                if (fourthBytes.Length < 32)
                {
                    return new ServiceResult<MessageModel>
                    {
                        Code = 400,
                        Message = "fourth should be atleast 32 bytes in length"
                    };
                }
            }
            catch (FormatException)
            {
                return new ServiceResult<MessageModel>
                {
                    Code = 400,
                    Message = "fourth is not a valid base64 string"
                };
            }

            message.TimeStamp = DateTime.Now.Ticks;
            await _publisher.PublishEvent(new EventModel<object>
            {
                Payload = (object)message,
                TimeStamp = message.TimeStamp,
                Type = EventTypes.SendMessage,
                ChannelID = message.ChannelID,
            });

            return new ServiceResult<MessageModel>
            {
                Code = 201,
                Data = message,
                Message = "success",
            };
        }
    }
}