

using Microsoft.Extensions.DependencyInjection;

namespace Services
{

    public static class MessageServiceExtensions
    {
        public static IServiceCollection AddMessageService(this IServiceCollection service)
        {
            service.AddSingleton<MessageService>();
            return service;
        }
    }
}