
using Services.Models;
using System.Threading.Tasks;

namespace Services.Stores
{

    public interface IChannelStore
    {
        public Task<ChannelModel> SaveChannel(ChannelModel newChannel);

        public Task<ChannelModel> GetChannel(string ChannelID);

        public Task<ChannelModel> DeleteChannel(string ChannelID);
    }
}