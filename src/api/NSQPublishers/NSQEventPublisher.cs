using Services.Models;
using Services.Publishers;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NsqSharp;
using System.Text.Json;

namespace NSQPublishers
{

    public class NSQEventPublisher : IEventPublisher
    {

        private Producer _producer;

        public NSQEventPublisher(string config)
        {
            _producer = new Producer(config);

        }
        public async Task PublishEvent(EventModel<object> model)
        {
            var payload = model.Payload;

            await _producer.PublishAsync($"{model.ChannelID}#ephemeral", JsonSerializer.Serialize(model, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DictionaryKeyPolicy = JsonNamingPolicy.CamelCase
            }));
        }
    }
}