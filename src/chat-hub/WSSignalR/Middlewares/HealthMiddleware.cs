using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;


namespace WSSignalR.Middlewares
{

    public class HealthMiddleware
    {
        private readonly RequestDelegate _next;
        
        public HealthMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext context)
        {
            await _invoke(context);
        }

        public async Task TaskInvokeAsync(HttpContext context) {
            await _invoke(context);
        }

        private async Task _invoke(HttpContext context) { 
            if (context.Request.Path == "/health" || context.Request.Path == "/health/")
            {
                context.Response.StatusCode = 200;
            }
            else
            {
                await _next(context);
            }
        }
    }

    public static class HealthMiddlewareExtensions
    {
        public static IApplicationBuilder UseHealthMiddleware(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<HealthMiddleware>();
            return builder;
        }
    }
}