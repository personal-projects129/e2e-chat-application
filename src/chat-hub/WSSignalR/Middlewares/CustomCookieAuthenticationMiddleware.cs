using RedisStores;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Text.Encodings.Web;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Services.Stores;
using System.Security.Claims;
using System;
using System.Text;
using System.Text.Unicode;
using System.Web;


namespace WSSignalR.Middlewares
{


    public class CustomCookieAuthenticationMiddleware : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private IChannelStore _store;
        private ILogger _logger;

        public CustomCookieAuthenticationMiddleware(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, IChannelStore store) : base(options, logger, encoder, clock)
        {
            _store = store;
            _logger = logger.CreateLogger<CustomCookieAuthenticationMiddleware>();

        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {

            string cookieValue;
            if (!Context.Request.Cookies.TryGetValue("chat_session", out cookieValue))
            {
                return AuthenticateResult.Fail("missing session cookie");
            }

            _logger.LogDebug(cookieValue);
            cookieValue = HttpUtility.UrlDecode(cookieValue);
            _logger.LogDebug(cookieValue);
            try
            {
                cookieValue = Encoding.UTF8.GetString(Convert.FromBase64String(cookieValue));
            }
            catch (Exception)
            {
                return AuthenticateResult.Fail("invalid cookie");
            }

            var splittedCookieValue = cookieValue.Split(":");
            if (splittedCookieValue.Length != 3)
            {
                return AuthenticateResult.Fail("invalid cookie");
            }

            var username = splittedCookieValue[0];
            var channelID = splittedCookieValue[1];
            var token = splittedCookieValue[2];

            _logger.LogDebug($"username: {username}, channelID: {channelID}, token: {token}");

            var channel = await _store.GetChannel(channelID);
            if (channel == null) {
                return AuthenticateResult.Fail("channel does not exist");
            }

            if (channel.Token != token) {
                return AuthenticateResult.Fail("invalid token");
            }


            return AuthenticateResult.Success(new AuthenticationTicket(
                new ClaimsPrincipal(
                    new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.Name, username),
                        new Claim(ClaimTypes.NameIdentifier, username),
                        new Claim("channelID", channelID)
                    },
                    nameof(CustomCookieAuthenticationMiddleware))
            ), "session"));
        }
    }
}