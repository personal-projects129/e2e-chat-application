
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NsqSharp;
using WSSignalR.Models;
using System;

using System.Text.Json;



namespace WSSignalR.Hubs
{

    public class ChatHub : Hub
    {

        private ILogger _logger;
        private Consumer _consumer;

        private string[] _nsqds;

        public ChatHub(ILogger<ChatHub> logger, ConfigurationModel config)
        {
            _logger = logger;
            _nsqds = config.NSQDs;
        }

        public override Task OnConnectedAsync()
        {
            _logger.LogInformation($"Client connected: {Context.ConnectionId}");
            
            var channelID = Context.User.FindFirst("channelID").Value;
            var connectionID = Context.ConnectionId;
            var client = Clients.Caller;
            return Task.Run(() =>
            {
                _consumer = new Consumer($"{channelID}#ephemeral", $"{connectionID}#ephemeral");
                _consumer.AddHandler(new ChatHandler(client, _logger));
                _consumer.ChangeMaxInFlight(_nsqds.Length);
                _consumer.ConnectToNsqd(_nsqds);
            });
        }

        public override async Task OnDisconnectedAsync(Exception exception) {
            if (exception != null) {
                _logger.LogCritical(exception.Message);
                _logger.LogCritical(exception.StackTrace);
            }

            await _consumer.StopAsync();
        }


        private class ChatHandler : IHandler
        {
            private IClientProxy _client;
            private ILogger _logger;

            public ChatHandler(IClientProxy client, ILogger logger)
            {
                _client = client;
                _logger = logger;
            }

            public async void HandleMessage(IMessage message)
            {
                await _client.SendAsync("ReceivedMessage", JsonSerializer.Deserialize<object>(message.Body));
                message.Finish();
            }

            public void LogFailedMessage(IMessage message)
            {
                _logger.LogWarning($"Message failed: ", message.Id);
            }
        }

    }
}