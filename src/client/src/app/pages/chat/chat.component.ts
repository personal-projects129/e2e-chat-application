import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren, AfterViewInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HubConnectionBuilder } from "@microsoft/signalr";
import { ChatSession } from "src/app/models/ChatSession";
import { Message } from "src/app/models/Message";
import { ChatService } from "src/app/services/chat.service";

@Component({
    selector: "chat",
    templateUrl: "./chat.component.html",
    styleUrls: ["./chat.component.scss"],
})
export class ChatComponent implements OnInit, AfterViewInit {

    @ViewChild("messagesDiv")
    messagesDiv!: ElementRef<HTMLDivElement>;
    @ViewChildren("messagesLi")
    messagesLi!: QueryList<HTMLLIElement>;

    chatSession!: ChatSession
    messages = new Array<Message>()
    invitehost = `${window.location.protocol}//${window.location.host}/join`
    showInvite = false
    constructor(
        private chatService: ChatService,
        private router: Router
    ) { }

    
    ngOnInit() {
        try {
            this.chatSession = this.chatService.GetChatSession()
        } catch (e) {
            this.router.navigateByUrl("/")
            return
        }

       
    }

    ngAfterViewInit() {
        this.chatService.ListenToEvents().subscribe(event => {
            if (event.type === "SendMessage") {
                this.messages.push(event.payload)
            }
        })

        this.messagesLi.changes.subscribe(() => {
            this.messagesDiv.nativeElement.scrollTop = this.messagesDiv.nativeElement.scrollHeight;
        })
    }

    onClickSend(textArea: HTMLTextAreaElement) {
        let message = textArea.value.trim()
        textArea.value = ""
        if (message === "") {
            return
        }

        this.chatService.CreateMessage(message).toPromise()
    }

    async onTextAreaKeyPress(event: KeyboardEvent) {
        if (event.key === "Enter" && event.shiftKey === false) {
            event.preventDefault()
            this.onClickSend(event.currentTarget as HTMLTextAreaElement)
            return
        }
    }

    onToggleInvite() {
        this.showInvite = !this.showInvite
    }

}