

export type EventModel<T> = {
    type: "SendMessage",
    timeStamp: number,
    channelID: string,
    payload: T,
}