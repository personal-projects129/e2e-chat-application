

export type ChatSession = {
    channelID: string,
    username: string,
    password: string
}