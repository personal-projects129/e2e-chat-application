import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GitlabBadgeComponent } from './gitlab-badge.component';

describe('GitlabBadgeComponent', () => {
  let component: GitlabBadgeComponent;
  let fixture: ComponentFixture<GitlabBadgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GitlabBadgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GitlabBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
